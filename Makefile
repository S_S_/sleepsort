CFLAGS = -Wall -Wextra -std=c11 -O2
LFLAGS = -lpthread
PROG = ssort
CXX = gcc

all: $(PROG)

$(PROG):
	$(CXX) -o $(PROG) $(CFLAGS) $(LFLAGS) main.c

clean:
	rm $(PROG)
