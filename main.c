#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

void* sleepsort (void *vargp)
{
	int arg = atoi((char *)vargp);
	sleep(arg);
	printf("%d\n", arg);
	return NULL;
}

int main(int argc, char *argv[])
{
	int n_sort = argc - 1;
	pthread_t *tid = calloc(n_sort, sizeof(pthread_t));

	for (int i = 1; i<argc; ++i)
	{
		pthread_create(&tid[i-1], NULL, sleepsort, (void*)argv[i]);
	}
	pthread_exit(NULL); // Exit main and let the rest do their jobs.
	free(tid);
	return 0;
}
